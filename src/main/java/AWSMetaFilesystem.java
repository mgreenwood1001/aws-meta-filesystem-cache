import net.fusejna.FuseException;

import java.io.IOException;

/**
 * Either a fuse based filesystem or a cache, this implementation allows the caller to decide based on the parameters
 * passed into the tool.
 * <p/>
 * For a fuse based filesystem specify:
 * <p/>
 * AWSMetaFilesystem fuse <i>mountpoint</i>
 * <p/>
 * For a cached based filesystem specify:
 * <p/>
 * AWSMetaFilesystem cache <i>cache location</i>
 * <p/>
 * @author mgreenwood <matt@mjgreenwood.net>
 * @since 2015-01-25
 */
public class AWSMetaFilesystem {
    public static void main(String args[]) throws FuseException, IOException {
        if (args.length != 2) {
            System.out.println("Usage:\n\tAWSMetaFilesystem fuse <mountpoint>\nOR\n\tAWSMetaFilesystem cache <mountpoint>");
        }
        if (args[0].equalsIgnoreCase("fuse")) {
            AWSMetaFuse fuse = new AWSMetaFuse();
            fuse.execute(args[1]);
        } else {
            AWSMetaFilesystemFSCache cache = new AWSMetaFilesystemFSCache();
            cache.execute(args[1]);
        }
    }
}
