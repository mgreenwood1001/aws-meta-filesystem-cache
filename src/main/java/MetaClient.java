import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * Defines an accessor for grabbing HTTP content from the metadata service for both content & size
 *
 * @author mgreenwood <matt@mjgreenwood.net>
 * @since 2015-01-25
 */
public class MetaClient {
    public final static String URI_ROOT = "http://169.254.169.254/latest/meta-data/";

    private final String root;

    public MetaClient() {
        this.root = URI_ROOT;
    }

    public MetaClient(String root) {
        this.root = root;
    }

    /**
     * Pull the content from the URI as a string
     *
     * @param uri - where root is /latest/meta-data
     * @return either the content of the object or empty string
     */
    public String getContent(String uri) {
        try {
            URL url = new URL(root + uri);
            URLConnection yc = url.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
            String inputLine;
            StringBuilder sb = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                sb.append(inputLine + "\n");
            }
            in.close();
            return sb.toString();
        } catch (IOException ioe) {
        }
        return "";
    }

    /**
     * Retrieve the content size by asking for HEAD length
     *
     * @param path
     * @return
     */
    public Integer getSize(String path) {
        HttpURLConnection conn = null;
        try {
            URL url = new URL(root + path);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("HEAD");
            conn.getInputStream();
            return conn.getContentLength();
        } catch (IOException e) {
            return -1;
        } finally {
            conn.disconnect();
        }
    }
}
