import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Simple utility to dump the Metadata service contents to the filesystem so that scripts can consume the content
 * rather than using curl/wget directly.  This cache should be recycled via cron on a regular basis.
 *
 * @author mgreenwood <matt@mjgreenwood.net>
 * @since 2015-01-25
 */
public class AWSMetaFilesystemFSCache {

    private MetaClient client = new MetaClient("http://169.254.169.254/latest");

    public void execute(String path) throws IOException {
        walk("/meta-data/", path);
        walk("/dynamic/", path);
        walk("/user-data", path);
    }

    public static void dump(String fullPath, String content) throws IOException {
        File file = new File(fullPath);
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        bw.write(content);
        bw.close();
    }

    public void walk(String linkRoot, String fsRoot) throws IOException {
        Queue<String> queueLinks = new LinkedList<String>();
        queueLinks.offer(linkRoot);
        while (!queueLinks.isEmpty()) {
            String link = queueLinks.poll();
            String result = client.getContent(link);
            System.out.println(link);
            if (link.endsWith("/") && !link.equalsIgnoreCase("/") && !link.isEmpty()) {
                File dir = new File(fsRoot + File.separator + link);
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                for (String line : result.split("\n")) {
                    if (!line.isEmpty()) {
                        queueLinks.offer(link + line);
                    }
                }
            } else {
                dump(fsRoot + File.separator + link, result);
            }
        }
    }
}
