import net.fusejna.DirectoryFiller;
import net.fusejna.ErrorCodes;
import net.fusejna.FuseException;
import net.fusejna.StructFuseFileInfo.FileInfoWrapper;
import net.fusejna.StructStat.StatWrapper;
import net.fusejna.types.TypeMode.NodeType;
import net.fusejna.util.FuseFilesystemAdapterFull;

import java.io.File;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.HashSet;

/**
 * A Fuse based filesystem for the metadata service in AWS
 *
 * @author mgreenwood <matt@mjgreenwood.net>
 * @since 2015-01-25
 */
public class AWSMetaFuse extends FuseFilesystemAdapterFull {

    private HashSet<String> directories = new HashSet<String>();
    private HashMap<String, Integer> files = new HashMap<String, Integer>();
    private MetaClient client = new MetaClient();

    public void execute(String path) throws FuseException {
        log(true).mount(path);
    }

    @Override
    public int getattr(final String path, final StatWrapper stat) {
        if (path.equals(File.separator)) { // Root directory
            stat.setMode(NodeType.DIRECTORY);
            return 0;
        }
        if (directories.contains(path + "/")) {
            stat.setMode(NodeType.DIRECTORY);
            return 0;
        }
        if (files.containsKey(path)) {
            stat.setMode(NodeType.FILE).size(files.get(path));
            return 0;
        }
        return -ErrorCodes.ENOENT();
    }

    @Override
    public int read(final String path, final ByteBuffer buffer, final long size, final long offset, final FileInfoWrapper info) {
        // Compute substring that we are being asked to read
        String content = client.getContent(path);
        final String s = content.substring((int) offset,
                (int) Math.max(offset, Math.min(content.length() - offset, offset + size)));
        buffer.put(s.getBytes());
        return s.getBytes().length;
    }

    @Override
    public int readdir(final String link, final DirectoryFiller filler) {
        String result = client.getContent(link);
        for (String line : result.split("\n")) {
            if (!line.isEmpty()) {
                String path = link + "/" + line;
                path = path.replaceAll("//", "/");
                // keep track of whether a location is a file or a directory as this can't be encoded in the Fuse string passed back
                // to the library
                if (line.endsWith("/")) {
                    directories.add(path);
                } else {
                    files.put(path, client.getSize(path));
                }
                filler.add(path);
            }
        }
        return 0;
    }

}
