awsMetaCache
============

An alternative approach to retrieving data from the metadata service in cloud stacks such as AWS and Digital Ocean that provide
instance level metadata thru a service hosted on a private network address (169.254.169.254).

This utility has no external dependencies beyond the Java runtime on the EC2 instance and lib-fuse if you
choose to use the fuse portion of the utility (the cache has no dependence on fuse).

* **Fast** faster than wget/curl every occasion where metadata is needed (fairly static information)

Please know that access keys, etc. that are available thru this utility should be made accessible only to the services that need
access to that data.  chown/chmod the directory and files created via running this script - ideally the service user that needs
this data will run this script.   Additionally, this data can become stale over time - use cron for any use cases where the data
is intended to be accessed beyond the initial setup of the node/resource.

This tool also provides a FUSE based filesystem for Linux.  You must have installed lib-fuse in order for
this to work.  The JNA library is already pre-bundled with the zip.

Usage
-----


### FUSE: Command line execution

Background the job for fuse, as the fuse filesystem is only available as long as the tool is running

```
unzip awsMetaCache-1.0.zip
awsMetaCache-1.0/bin/awsMetaCache fuse <mountpoint> &
```

### Cache: Command line execute

```
unzip awsMetaCache-1.0.zip
awsMetaCache-1.0/bin/awsMetaCache cache <pathToCache>
```

The path to the cache directory must already exist and be writable via the current user.

Download
--------

[awsMetaCache-1.0.zip](https://bitbucket.org/mgreenwood1001/aws-meta-filesystem-cache/src/67d8fce268584af3fe610947fd5cb4a52c3a910f/build/distributions/awsMetaCache-1.0.zip?at=master)

Build
-----

To build awsMetaCache on your local machine, have a recent version of gradle installed.  Checkout the repository and invoke
gradle without arguments.  The build zip artifact will be in build/distributions.

License
-------
The code is available under the terms of the [MIT License](http://opensource.org/licenses/MIT).
